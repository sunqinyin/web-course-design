# WEB-课程设计


## 1.项目技术

- 数据库：MySQL8.0
- 数据库设计软件：Power Designer16.5
- IDE：IDEA
- Web容器：Apache Tomcat 8.5
- 项目管理工具：Maven
- 后端技术：Spring+Spring MVC+MyBatis（SSM框架）
- 前端技术：**LayUI**（Vue/React）


