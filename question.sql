/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.7.33-log : Database - question
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`question` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `question`;

/*Table structure for table `logtable` */

DROP TABLE IF EXISTS `logtable`;

CREATE TABLE `logtable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `operateor` varchar(5) DEFAULT NULL,
  `operateType` varchar(20) DEFAULT NULL,
  `operateDate` datetime DEFAULT NULL,
  `operateResult` varchar(4) DEFAULT NULL,
  `remark` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

/*Data for the table `logtable` */

insert  into `logtable`(`id`,`operateor`,`operateType`,`operateDate`,`operateResult`,`remark`) values (5,'admin','用户登录','2020-12-28 20:42:18','正常',NULL),(6,'admin','添加用户','2020-12-28 20:43:01','正常',NULL),(7,'admin','添加用户','2020-12-28 20:43:18','正常',NULL),(8,'admin','用户更新','2020-12-28 20:43:28','正常',NULL),(9,'admin','删除用户','2020-12-28 20:43:31','正常',NULL),(10,'admin','用户登录','2020-12-28 21:01:50','正常',NULL),(11,'admin','用户登录','2020-12-28 21:10:10','正常',NULL),(12,'admin','用户登录','2020-12-28 21:15:39','正常',NULL),(13,'admin','用户登录','2020-12-28 21:18:24','正常',NULL),(14,'admin','用户登录','2020-12-28 21:23:34','正常',NULL),(15,'admin','用户登录','2020-12-28 21:23:35','正常',NULL),(16,'admin','用户登录','2020-12-28 21:33:57','正常',NULL),(17,'admin','用户登录','2020-12-28 21:40:04','正常',NULL),(18,'admin','用户登录','2020-12-28 21:45:11','正常',NULL),(19,'admin','用户登录','2020-12-30 22:09:43','正常',NULL),(20,'admin','用户登录','2021-01-08 13:20:50','正常',NULL),(21,'admin','用户登录','2021-01-08 14:09:09','正常',NULL),(22,'admin','用户登录','2021-01-08 19:55:57','正常',NULL),(23,'admin','用户登录','2021-01-12 08:14:23','正常',NULL),(24,'admin','用户更新','2021-01-12 08:19:31','正常',NULL),(25,'admin','删除用户','2021-01-12 08:19:38','正常',NULL),(26,'admin','用户登录','2021-01-12 15:02:41','正常',NULL),(27,'admin','添加用户','2021-01-12 15:03:01','正常',NULL),(28,'admin','删除用户','2021-01-12 15:03:11','正常',NULL),(29,'admin','用户登录','2021-01-12 16:16:07','正常',NULL),(30,'admin','添加用户','2021-01-12 16:16:36','正常',NULL),(31,'admin','用户更新','2021-01-12 16:16:45','正常',NULL),(32,'admin','用户登录','2021-05-08 16:42:26','正常',NULL),(33,'admin','用户登录','2021-05-08 16:42:35','正常',NULL),(34,'admin','用户登录','2021-05-09 19:48:44','正常',NULL),(35,'admin','用户登录','2021-05-10 15:08:06','正常',NULL),(36,'admin','添加用户','2021-05-10 15:15:26','正常',NULL),(37,'admin','删除用户','2021-05-10 15:15:36','正常',NULL),(38,'admin','用户更新','2021-05-10 15:15:42','正常',NULL),(39,'admin','用户登录','2021-05-26 21:18:55','正常',NULL),(40,'admin','用户登录','2021-05-26 08:33:59','正常',NULL);

/*Table structure for table `tb_admin` */

DROP TABLE IF EXISTS `tb_admin`;

CREATE TABLE `tb_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `remark` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2098 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `tb_admin` */

insert  into `tb_admin`(`id`,`account`,`password`,`name`,`phone`,`remark`) values (2085,'xiaoming','123333','小明','2222','23213213'),(2086,'xioahu','123123','xiaohon','222','334234'),(2087,'xiaozhang','23232','小张','444','444'),(2088,'xiaofei','ewe','小飞','4','4'),(2090,'1121','e10adc3949ba59abbe56e057f20f883e','胡图图','18533333333','芜湖'),(2092,'小王子','e10adc3949ba59abbe56e057f20f883e','小王子','18533333333','小王子'),(2094,'123123123','e10adc3949ba59abbe56e057f20f883e','3123123','18533333333','3123'),(2095,'admin','e10adc3949ba59abbe56e057f20f883e','admin','18533333333','18533333333'),(2096,'张三','e10adc3949ba59abbe56e057f20f883e','张三','18533333333','张三1111'),(2097,'hututu1111','e10adc3949ba59abbe56e057f20f883e','胡图图','18533333333','无');

/*Table structure for table `tb_answer_opt` */

DROP TABLE IF EXISTS `tb_answer_opt`;

CREATE TABLE `tb_answer_opt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `survey_id` int(11) DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL,
  `opt_id` int(11) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL COMMENT '1radio|2checkbox',
  `create_time` datetime DEFAULT NULL,
  `voter` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_Reference_2` (`opt_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `tb_answer_opt` */

insert  into `tb_answer_opt`(`id`,`survey_id`,`question_id`,`opt_id`,`type`,`create_time`,`voter`) values (43,28,67,218,'1','2020-12-28 11:33:26','d07b82a1-e669-4cdf-8cdc-a6f6eb58c19f'),(44,28,68,222,'2','2020-12-28 11:33:26','d07b82a1-e669-4cdf-8cdc-a6f6eb58c19f'),(45,28,68,223,'2','2020-12-28 11:33:26','d07b82a1-e669-4cdf-8cdc-a6f6eb58c19f'),(46,28,67,218,'1','2020-12-28 13:44:08','a6329f4e-fb84-4614-8191-e7075364bc35'),(47,28,68,222,'2','2020-12-28 13:44:08','a6329f4e-fb84-4614-8191-e7075364bc35'),(48,29,70,226,'1','2020-12-28 13:51:13','36fc634c-3327-4228-80d1-b4b1ce7fc773'),(49,29,71,240,'1','2020-12-28 13:51:13','36fc634c-3327-4228-80d1-b4b1ce7fc773'),(50,29,72,235,'2','2020-12-28 13:51:13','36fc634c-3327-4228-80d1-b4b1ce7fc773'),(51,29,72,237,'2','2020-12-28 13:51:13','36fc634c-3327-4228-80d1-b4b1ce7fc773'),(52,29,72,238,'2','2020-12-28 13:51:13','36fc634c-3327-4228-80d1-b4b1ce7fc773'),(53,30,74,246,'1','2020-12-28 16:44:29','873fd00e-4b8b-428a-9699-9b7bc03b4ade'),(54,30,75,248,'2','2020-12-28 16:44:29','873fd00e-4b8b-428a-9699-9b7bc03b4ade'),(55,30,75,249,'2','2020-12-28 16:44:29','873fd00e-4b8b-428a-9699-9b7bc03b4ade'),(56,30,75,251,'2','2020-12-28 16:44:29','873fd00e-4b8b-428a-9699-9b7bc03b4ade'),(57,30,75,252,'2','2020-12-28 16:44:29','873fd00e-4b8b-428a-9699-9b7bc03b4ade'),(58,30,76,253,'2','2020-12-28 16:44:29','873fd00e-4b8b-428a-9699-9b7bc03b4ade'),(59,30,76,254,'2','2020-12-28 16:44:29','873fd00e-4b8b-428a-9699-9b7bc03b4ade'),(60,30,76,255,'2','2020-12-28 16:44:29','873fd00e-4b8b-428a-9699-9b7bc03b4ade'),(61,30,76,256,'2','2020-12-28 16:44:29','873fd00e-4b8b-428a-9699-9b7bc03b4ade'),(62,31,77,263,'1','2020-12-30 22:14:38','3b17f86a-8a09-4c71-9c9e-1c8d99f1ec74'),(63,31,78,265,'2','2020-12-30 22:14:38','3b17f86a-8a09-4c71-9c9e-1c8d99f1ec74'),(64,31,79,268,'2','2020-12-30 22:14:38','3b17f86a-8a09-4c71-9c9e-1c8d99f1ec74'),(65,31,79,269,'2','2020-12-30 22:14:38','3b17f86a-8a09-4c71-9c9e-1c8d99f1ec74'),(66,31,79,270,'2','2020-12-30 22:14:38','3b17f86a-8a09-4c71-9c9e-1c8d99f1ec74'),(67,31,77,260,'1','2020-12-30 22:15:13','75deef90-ea78-4e98-94fe-720121668e4d'),(68,31,78,264,'2','2020-12-30 22:15:13','75deef90-ea78-4e98-94fe-720121668e4d'),(69,31,79,269,'2','2020-12-30 22:15:13','75deef90-ea78-4e98-94fe-720121668e4d'),(70,31,79,270,'2','2020-12-30 22:15:13','75deef90-ea78-4e98-94fe-720121668e4d'),(71,32,82,272,'1','2021-01-08 14:11:40','5f4db940-f1fd-443e-ad19-db203d05f796'),(72,32,83,279,'2','2021-01-08 14:11:40','5f4db940-f1fd-443e-ad19-db203d05f796'),(73,32,85,282,'1','2021-01-08 14:11:40','5f4db940-f1fd-443e-ad19-db203d05f796'),(74,33,86,287,'1','2021-01-12 08:18:38','c66ed554-9b02-463d-ad18-fb2c2d4dfd7c'),(75,33,87,288,'1','2021-01-12 08:18:38','c66ed554-9b02-463d-ad18-fb2c2d4dfd7c'),(76,33,88,292,'2','2021-01-12 08:18:38','c66ed554-9b02-463d-ad18-fb2c2d4dfd7c'),(77,33,88,293,'2','2021-01-12 08:18:38','c66ed554-9b02-463d-ad18-fb2c2d4dfd7c'),(78,33,88,294,'2','2021-01-12 08:18:38','c66ed554-9b02-463d-ad18-fb2c2d4dfd7c'),(79,33,86,284,'1','2021-01-12 08:18:50','ce734ca2-8ccc-47b8-9b74-b8f522b01255'),(80,33,87,288,'1','2021-01-12 08:18:50','ce734ca2-8ccc-47b8-9b74-b8f522b01255'),(81,33,88,293,'2','2021-01-12 08:18:50','ce734ca2-8ccc-47b8-9b74-b8f522b01255'),(82,34,90,298,'1','2021-01-12 15:06:49','06fe979b-cf7e-432d-a529-a1c29395f57c'),(83,34,91,301,'2','2021-01-12 15:06:49','06fe979b-cf7e-432d-a529-a1c29395f57c'),(84,34,91,302,'2','2021-01-12 15:06:49','06fe979b-cf7e-432d-a529-a1c29395f57c'),(85,34,90,298,'1','2021-01-12 15:06:58','922097b6-3704-4fdc-9c7b-323a72faaeaf'),(86,34,91,301,'2','2021-01-12 15:06:58','922097b6-3704-4fdc-9c7b-323a72faaeaf'),(87,36,93,308,'1','2021-01-12 16:21:10','d35b350d-5ac2-480a-8b12-7f93107a070c'),(88,36,94,309,'2','2021-01-12 16:21:10','d35b350d-5ac2-480a-8b12-7f93107a070c'),(89,36,94,311,'2','2021-01-12 16:21:10','d35b350d-5ac2-480a-8b12-7f93107a070c'),(90,36,94,312,'2','2021-01-12 16:21:10','d35b350d-5ac2-480a-8b12-7f93107a070c'),(91,36,95,315,'1','2021-01-12 16:21:10','d35b350d-5ac2-480a-8b12-7f93107a070c'),(92,36,93,305,'1','2021-01-12 16:21:32','f3735a97-6685-4eee-9798-548961a90c4a'),(93,36,94,309,'2','2021-01-12 16:21:32','f3735a97-6685-4eee-9798-548961a90c4a'),(94,36,95,315,'1','2021-01-12 16:21:32','f3735a97-6685-4eee-9798-548961a90c4a'),(95,37,97,318,'1','2021-05-10 15:14:30','da654309-4d7c-4512-a228-15c6e3c258d7'),(96,37,98,322,'2','2021-05-10 15:14:30','da654309-4d7c-4512-a228-15c6e3c258d7'),(97,37,98,323,'2','2021-05-10 15:14:30','da654309-4d7c-4512-a228-15c6e3c258d7'),(98,37,97,318,'1','2021-05-10 15:14:42','303457a8-5ccd-4d48-8fb9-3a9dcf78b3f6'),(99,37,98,321,'2','2021-05-10 15:14:42','303457a8-5ccd-4d48-8fb9-3a9dcf78b3f6');

/*Table structure for table `tb_answer_txt` */

DROP TABLE IF EXISTS `tb_answer_txt`;

CREATE TABLE `tb_answer_txt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `survey_id` int(11) DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL,
  `result` varchar(200) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `voter` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `tb_answer_txt` */

insert  into `tb_answer_txt`(`id`,`survey_id`,`question_id`,`result`,`create_time`,`voter`) values (16,28,69,'111','2020-12-28 11:33:26','d07b82a1-e669-4cdf-8cdc-a6f6eb58c19f'),(17,28,69,'1','2020-12-28 13:44:08','a6329f4e-fb84-4614-8191-e7075364bc35'),(18,29,73,'暂时没有','2020-12-28 13:51:13','36fc634c-3327-4228-80d1-b4b1ce7fc773'),(19,31,80,'1','2020-12-30 22:14:38','3b17f86a-8a09-4c71-9c9e-1c8d99f1ec74'),(20,31,81,'1','2020-12-30 22:14:38','3b17f86a-8a09-4c71-9c9e-1c8d99f1ec74'),(21,31,80,'','2020-12-30 22:15:13','75deef90-ea78-4e98-94fe-720121668e4d'),(22,31,81,'','2020-12-30 22:15:13','75deef90-ea78-4e98-94fe-720121668e4d'),(23,32,84,'','2021-01-08 14:11:40','5f4db940-f1fd-443e-ad19-db203d05f796'),(24,33,89,'','2021-01-12 08:18:38','c66ed554-9b02-463d-ad18-fb2c2d4dfd7c'),(25,33,89,'','2021-01-12 08:18:50','ce734ca2-8ccc-47b8-9b74-b8f522b01255'),(26,34,92,'大多数','2021-01-12 15:06:49','06fe979b-cf7e-432d-a529-a1c29395f57c'),(27,34,92,'','2021-01-12 15:06:58','922097b6-3704-4fdc-9c7b-323a72faaeaf'),(28,36,96,'学习','2021-01-12 16:21:10','d35b350d-5ac2-480a-8b12-7f93107a070c'),(29,36,96,'学习','2021-01-12 16:21:32','f3735a97-6685-4eee-9798-548961a90c4a'),(30,37,99,'无','2021-05-10 15:14:30','da654309-4d7c-4512-a228-15c6e3c258d7'),(31,37,99,'无','2021-05-10 15:14:42','303457a8-5ccd-4d48-8fb9-3a9dcf78b3f6');

/*Table structure for table `tb_question` */

DROP TABLE IF EXISTS `tb_question`;

CREATE TABLE `tb_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `remark` varchar(200) DEFAULT NULL,
  `type` int(1) DEFAULT NULL COMMENT '1radio|2checkbox|3text|4textarea',
  `required` int(1) DEFAULT NULL COMMENT '0非必填1必填',
  `check_style` varchar(50) DEFAULT NULL COMMENT 'text;number;date',
  `order_style` int(1) DEFAULT NULL COMMENT '0顺序1随机',
  `show_style` int(1) DEFAULT NULL COMMENT '1;2;3;4',
  `test` int(1) DEFAULT NULL COMMENT '0不测评1测评',
  `score` int(3) DEFAULT NULL,
  `orderby` int(11) DEFAULT NULL,
  `creator` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `survey_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_Reference_1` (`survey_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `tb_question` */

insert  into `tb_question`(`id`,`title`,`remark`,`type`,`required`,`check_style`,`order_style`,`show_style`,`test`,`score`,`orderby`,`creator`,`create_time`,`survey_id`) values (64,'你的年级','',1,1,NULL,NULL,NULL,0,0,NULL,2073,'2020-12-28 10:27:00',26),(65,'晚上几点睡觉','',1,1,NULL,NULL,NULL,0,0,NULL,2073,'2020-12-28 10:27:48',26),(66,'晚上在干什么','',2,1,NULL,NULL,NULL,0,0,NULL,2073,'2020-12-28 10:28:25',26),(67,'标题','描述',1,1,NULL,NULL,NULL,0,0,NULL,2073,'2020-12-28 10:36:39',28),(68,'标题','描述',2,1,NULL,NULL,NULL,0,0,NULL,2073,'2020-12-28 10:36:41',28),(69,'标题','描述',3,1,NULL,NULL,NULL,NULL,NULL,NULL,2073,'2020-12-28 10:36:43',28),(70,'你所在的年级','',1,1,NULL,NULL,NULL,0,0,NULL,2073,'2020-12-28 13:47:40',29),(71,'一般正常晚上记得睡觉','',1,1,NULL,NULL,NULL,0,0,NULL,2073,'2020-12-28 13:48:26',29),(72,'晚上一般都在做什么','',2,1,NULL,NULL,NULL,0,0,NULL,2073,'2020-12-28 13:49:10',29),(73,'期望的自己的安排','',4,1,NULL,NULL,NULL,NULL,NULL,NULL,2073,'2020-12-28 13:49:59',29),(74,'手机的价格区间','',1,1,NULL,NULL,NULL,0,0,NULL,2073,'2020-12-28 16:32:06',30),(75,'喜欢的手机品牌','',2,1,NULL,NULL,NULL,0,0,NULL,2073,'2020-12-28 16:32:59',30),(76,'手机经常干什么','',2,1,NULL,NULL,NULL,0,0,NULL,2073,'2020-12-28 16:33:57',30),(77,'你是大几的','',1,1,NULL,NULL,NULL,0,0,NULL,2073,'2020-12-30 22:12:18',31),(78,'你多大','描述',2,1,NULL,NULL,NULL,0,0,NULL,2073,'2020-12-30 22:12:46',31),(79,'为什么喜欢','描述',2,1,NULL,NULL,NULL,0,0,NULL,2073,'2020-12-30 22:12:55',31),(80,'标题1111','描述',3,1,NULL,NULL,NULL,NULL,NULL,NULL,2073,'2020-12-30 22:12:59',31),(81,'22222','描述',4,1,NULL,NULL,NULL,NULL,NULL,NULL,2073,'2020-12-30 22:13:03',31),(82,'标题111','描述1',1,1,NULL,NULL,NULL,0,0,NULL,2073,'2021-01-08 14:10:07',32),(83,'标题2','描述2',2,1,NULL,NULL,NULL,0,0,NULL,2073,'2021-01-08 14:10:12',32),(84,'标题2','描述2',3,1,NULL,NULL,NULL,NULL,NULL,NULL,2073,'2021-01-08 14:10:16',32),(85,'标题2','描述',1,1,NULL,NULL,NULL,0,0,NULL,2073,'2021-01-08 14:10:20',32),(86,'你是大几的学生','',1,1,NULL,NULL,NULL,0,0,NULL,2073,'2021-01-12 08:15:57',33),(87,'每天在哪里吃','',1,1,NULL,NULL,NULL,0,0,NULL,2073,'2021-01-12 08:16:26',33),(88,'你最喜欢吃什么','',2,1,NULL,NULL,NULL,0,0,NULL,2073,'2021-01-12 08:16:50',33),(89,'你的姓名','',3,1,NULL,NULL,NULL,NULL,NULL,NULL,2073,'2021-01-12 08:17:03',33),(90,'你大几','',1,1,NULL,NULL,NULL,0,0,NULL,2073,'2021-01-12 15:05:39',34),(91,'一般几点三角','描述',2,1,NULL,NULL,NULL,0,0,NULL,2073,'2021-01-12 15:05:53',34),(92,'标题22222','描述',3,1,NULL,NULL,NULL,NULL,NULL,NULL,2073,'2021-01-12 15:05:56',34),(93,'你是大几的',NULL,1,1,NULL,NULL,NULL,0,0,NULL,2095,'2021-01-12 16:18:26',36),(94,'晚上一般在干什么',NULL,2,1,NULL,NULL,NULL,0,0,NULL,2095,'2021-01-12 16:18:59',36),(95,'晚上几点睡觉',NULL,1,1,NULL,NULL,NULL,0,0,NULL,2095,'2021-01-12 16:19:24',36),(96,'晚上想要干什么','描述',3,1,NULL,NULL,NULL,NULL,NULL,NULL,2095,'2021-01-12 16:19:37',36),(97,'你晚上几点睡觉','描述',1,1,NULL,NULL,NULL,0,0,NULL,2095,'2021-05-10 15:10:26',37),(98,'你晚上在干什么','描述',2,1,NULL,NULL,NULL,0,0,NULL,2095,'2021-05-10 15:11:00',37),(99,'你喜欢晚上干什么','描述',3,1,NULL,NULL,NULL,NULL,NULL,NULL,2095,'2021-05-10 15:11:12',37);

/*Table structure for table `tb_question_opt` */

DROP TABLE IF EXISTS `tb_question_opt`;

CREATE TABLE `tb_question_opt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `survey_id` int(11) DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL COMMENT '1radio|2checkbox',
  `opt` varchar(200) DEFAULT NULL,
  `orderby` int(11) DEFAULT NULL,
  `answer` int(1) DEFAULT NULL COMMENT '默认为NULL；1答案',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=326 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `tb_question_opt` */

insert  into `tb_question_opt`(`id`,`survey_id`,`question_id`,`type`,`opt`,`orderby`,`answer`) values (205,26,64,'1','大一',1,NULL),(206,26,64,'1','大二',2,NULL),(207,26,64,'1','大三',3,NULL),(208,26,64,'1','大四',4,NULL),(209,26,65,'1','10点之前',1,NULL),(210,26,65,'1','10-11',2,NULL),(211,26,65,'1','11-12·',3,NULL),(212,26,65,'1','12以后',4,NULL),(213,26,66,'2','学习',1,NULL),(214,26,66,'2','打游戏',2,NULL),(215,26,66,'2','运动',3,NULL),(216,26,66,'2','看剧',4,NULL),(217,26,66,'2','其他',5,NULL),(218,28,67,'1','选项',1,NULL),(219,28,67,'1','选项',2,NULL),(220,28,67,'1','选项',3,NULL),(221,28,67,'1','选项',4,NULL),(222,28,68,'2','选项',1,NULL),(223,28,68,'2','选项',2,NULL),(224,28,68,'2','选项',3,NULL),(225,28,68,'2','选项',4,NULL),(226,29,70,'1','大一',1,NULL),(227,29,70,'1','大二',2,NULL),(228,29,70,'1','大三',3,NULL),(229,29,70,'1','大四',4,NULL),(234,29,72,'2','打游戏',1,NULL),(235,29,72,'2','学习',2,NULL),(236,29,72,'2','看剧',3,NULL),(237,29,72,'2','锻炼身体',4,NULL),(238,29,72,'2','其他',5,NULL),(239,29,71,'1','10点之前',1,NULL),(240,29,71,'1','10:00-11:00',2,NULL),(241,29,71,'1','11:00-12:00',3,NULL),(242,29,71,'1','12:00之后',4,NULL),(243,30,74,'1','1000-2000',1,NULL),(244,30,74,'1','2000-3000',2,NULL),(245,30,74,'1','3000-4000',3,NULL),(246,30,74,'1','4000-5000',4,NULL),(247,30,74,'1','5000以上',5,NULL),(248,30,75,'2','华为',1,NULL),(249,30,75,'2','小米',2,NULL),(250,30,75,'2','oppo',3,NULL),(251,30,75,'2','1+',4,NULL),(252,30,75,'2','苹果',5,NULL),(253,30,76,'2','玩游戏',1,NULL),(254,30,76,'2','聊天',2,NULL),(255,30,76,'2','学习',3,NULL),(256,30,76,'2','听歌',4,NULL),(257,30,76,'2','看剧',5,NULL),(258,30,76,'2','看小说',6,NULL),(259,30,76,'2','新闻',7,NULL),(260,31,77,'1','大一',1,NULL),(261,31,77,'1','大二',2,NULL),(262,31,77,'1','大三',3,NULL),(263,31,77,'1','大四',4,NULL),(264,31,78,'2','18-20',1,NULL),(265,31,78,'2','20-22',2,NULL),(266,31,78,'2','22-25',3,NULL),(267,31,78,'2','25-10',4,NULL),(268,31,79,'2','选项',1,NULL),(269,31,79,'2','选项',2,NULL),(270,31,79,'2','选项',3,NULL),(271,31,79,'2','选项',4,NULL),(272,32,82,'1','选项',1,NULL),(273,32,82,'1','选项',2,NULL),(274,32,82,'1','选项',3,NULL),(275,32,82,'1','选项',4,NULL),(276,32,83,'2','选项2',1,NULL),(277,32,83,'2','选项2',2,NULL),(278,32,83,'2','选项2',3,NULL),(279,32,83,'2','选项2',4,NULL),(280,32,85,'1','选项',1,NULL),(281,32,85,'1','选项2',2,NULL),(282,32,85,'1','选项',3,NULL),(283,32,85,'1','选项',4,NULL),(284,33,86,'1','大一',1,NULL),(285,33,86,'1','大二',2,NULL),(286,33,86,'1','大三',3,NULL),(287,33,86,'1','大四',4,NULL),(288,33,87,'1','食堂',1,NULL),(289,33,87,'1','外面',2,NULL),(290,33,87,'1','外卖',3,NULL),(291,33,87,'1','随便糊弄',4,NULL),(292,33,88,'2','1',1,NULL),(293,33,88,'2','2',2,NULL),(294,33,88,'2','3',3,NULL),(295,33,88,'2','4<div></div>',4,NULL),(296,33,88,'2','',5,NULL),(297,34,90,'1','选项1',1,NULL),(298,34,90,'1','选项2',2,NULL),(299,34,90,'1','选项3',3,NULL),(300,34,90,'1','选项4',4,NULL),(301,34,91,'2','选项1',1,NULL),(302,34,91,'2','选项2',2,NULL),(303,34,91,'2','选项3',3,NULL),(304,34,91,'2','选项4',4,NULL),(305,36,93,'1','大一',1,NULL),(306,36,93,'1','大二',2,NULL),(307,36,93,'1','大三',3,NULL),(308,36,93,'1','大四',4,NULL),(309,36,94,'2','学习',1,NULL),(310,36,94,'2','打游戏',2,NULL),(311,36,94,'2','睡觉',3,NULL),(312,36,94,'2','锻炼',4,NULL),(313,36,95,'1','10点之前',1,NULL),(314,36,95,'1','10-11',2,NULL),(315,36,95,'1','11-12',3,NULL),(316,36,95,'1','12之后',4,NULL),(317,37,97,'1','1.10点之前',1,NULL),(318,37,97,'1','2. 10-11',2,NULL),(319,37,97,'1','3. 11-12',3,NULL),(320,37,97,'1','4. 12以后',4,NULL),(321,37,98,'2','学习',1,NULL),(322,37,98,'2','看电视',2,NULL),(323,37,98,'2','看小说',3,NULL),(324,37,98,'2','打游戏',4,NULL),(325,37,98,'2','运动',5,NULL);

/*Table structure for table `tb_survey` */

DROP TABLE IF EXISTS `tb_survey`;

CREATE TABLE `tb_survey` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `remark` varchar(200) DEFAULT NULL,
  `bounds` int(1) DEFAULT NULL COMMENT '0:不限制;1:限制',
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `rules` int(1) DEFAULT NULL COMMENT '0公开;1密码',
  `password` varchar(50) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL COMMENT '创建、执行中、结束',
  `logo` varchar(200) DEFAULT NULL,
  `bgimg` varchar(200) DEFAULT NULL,
  `anon` int(1) DEFAULT NULL COMMENT '0匿名；1不匿名',
  `creator` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `tb_survey` */

insert  into `tb_survey`(`id`,`title`,`remark`,`bounds`,`start_time`,`end_time`,`rules`,`password`,`url`,`state`,`logo`,`bgimg`,`anon`,`creator`,`create_time`) values (28,'aaaaaa','',0,'2020-12-27 10:36:27','2020-12-29 10:36:30',0,'','http://localhost:8080/dy/1a61b6bd-c221-432e-8961-6f882cb67c44','执行中',NULL,'10c1b68cee894dcba93dda727889c220_2.jpg',0,2073,'2020-12-28 10:36:31'),(29,'大学生几点睡觉','调查大学生几点睡觉',0,'2020-12-28 13:46:55','2020-12-28 13:46:59',0,'','http://localhost:8080/dy/de72dc31-8be8-4c01-94c3-066563654d60','结束',NULL,'ed675768edb648b69e2d539e6ae0b479_1.png',0,2073,'2020-12-28 13:47:00'),(30,'大学生手机使用调查','调查大学生手机的使用',0,'2020-12-28 16:20:47','2020-12-31 00:00:00',0,'123456','http://localhost:8080/dy/c949ae94-5c67-46f5-846f-408705c0724d','执行中',NULL,'8cc3ca86a5d24e0faea5822ba5955c33_1.png',0,2073,'2020-12-28 16:24:34'),(31,'你喜欢java这个课嘛','',0,'2020-12-30 22:11:00','2020-12-31 00:00:00',0,'','http://localhost:8080/dy/c49387e8-e7d8-4a5b-8572-5f863e566c95','执行中',NULL,'e025c15e662943059a47a26d3941d914_1.png',0,2073,'2020-12-30 22:11:18'),(33,'大学生每天的消费','',0,'2021-01-12 08:15:06','2021-01-19 00:00:00',0,'','http://localhost:8080/dy/b3cd1c5a-1070-4749-9208-e2347aa8e4b4','执行中',NULL,'79dec37c7fcc42c1a7c6fd9e68601fbb_1.png',0,2073,'2021-01-12 08:15:16'),(36,'大学生晚上几点睡觉1','调查大学生几点睡觉',0,'2021-01-12 16:17:33','2021-01-16 00:00:00',0,'1111','http://localhost:8081/dy/6f924c5f-91aa-47b8-88b0-b088a7ab9ed2','执行中',NULL,'a05ef8a8855c4a419a9655d74c439454_1.png',0,2095,'2021-01-12 16:17:46'),(37,'你晚上几点睡觉','调查大学生几点睡觉啊',0,'2021-05-10 15:08:38','2021-05-06 00:00:00',0,'','http://localhost:8081/dy/e674ae45-181c-4601-a248-942e3a804799','执行中',NULL,'a283e610fcc8418f8b961639da47975b_1.png',0,2095,'2021-05-10 15:09:08');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
