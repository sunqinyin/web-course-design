package com.huliyong.controller;

import cn.hutool.crypto.SecureUtil;
import com.github.pagehelper.PageInfo;
import com.huliyong.pojo.Admin;
import com.huliyong.service.AdminService;
import com.huliyong.utils.MapControl;
import com.huliyong.utils.Page;
import com.huliyong.vo.myResultMap;
//import com.sun.org.apache.regexp.internal.RE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private AdminService adminService;


    @RequestMapping("/deleteByPrimaryKey")
    @ResponseBody
    public int deleteByPrimaryKey(@RequestParam("id") Integer id){
        return adminService.deleteByPrimaryKey(id);
    }

    /**
     * 插入
     * @param admin
     * @return
     */
    @RequestMapping("/insertSelective")
    @ResponseBody
    public int insert(@RequestBody Admin admin){
        System.out.println(admin);
        return adminService.insert(admin);
    }
    /**
     *  更新  根据id
     * @param admin
     * @return
     */
    @RequestMapping("/updateByPrimaryKeySelective")
    @ResponseBody
    public int updateByPrimaryKeySelective(@RequestBody Admin admin){
        return adminService.updateByPrimaryKeySelective(admin);
    }

    /**
     * 根据id 查询
     * @param id
     * @return
     */
    @RequestMapping("/selectByPrimaryKey")
    @ResponseBody
    public Admin selectByPrimaryKey(@RequestParam("id") Integer id){
        return adminService.selectByPrimaryKey(id);
    }


    @RequestMapping("/qq")
    @ResponseBody
    public  String aa(Date date){
        System.out.println(date);
        return date.toString();
    }




    @GetMapping("/list")
    public String toList(){
        return "admin/list";
    }


    @PostMapping("/query")
    @ResponseBody
    public Map<String,Object> query( Admin admin,@RequestParam("page") int page ,@RequestParam("limit") int limit){
        System.out.println(admin);
        System.out.println(page+"  "+limit);
        PageInfo pageInfo = adminService.queryByPage(admin, page, limit);
        HashMap<String,Object> map = new HashMap<>();
        map.put("code",0);
        map.put("msg","");
        map.put("count",pageInfo.getTotal());
        map.put("data",pageInfo.getList());

        return  map;
    }

    @GetMapping("/create")
    public String tocreate(){
        return "admin/add";
    }


    @PostMapping("/create")
    @ResponseBody
    public Map<String,Object> create(@RequestBody Admin admin){
        admin.setPassword(SecureUtil.md5((admin.getPassword())));

        int result = adminService.insert(admin);
        if(result<=0){
            //失败的情况下
            return MapControl.getInstance().error().getMap();
        }
        return MapControl.getInstance().success().getMap();
    }


    @PostMapping("/delete")
    @ResponseBody
    public Map<String,Object> delete(String ids){
        int result = adminService.deleteBatch(ids);
        if(result<=0){
            //失败的情况下
            return MapControl.getInstance().error().getMap();
        }
        return MapControl.getInstance().success().getMap();
    }

    @GetMapping("/detail")
    public String detail(Integer id,ModelMap modelMap){
        Admin admin = adminService.selectByPrimaryKey(id);
        modelMap.addAttribute("admin",admin);
        return "admin/update";
    }



    @PostMapping("/update")
    @ResponseBody
    public Map<String, Object> update(@RequestBody Admin admin){
        System.out.println("更新");
        int result = adminService.updateByPrimaryKeySelective(admin);
        if(result<=0){
            //失败的情况下
            return MapControl.getInstance().error().getMap();
        }
        return MapControl.getInstance().success().getMap();
    }
}
