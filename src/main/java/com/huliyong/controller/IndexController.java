package com.huliyong.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * @Auther: H_L_Y
 * @Date: 2020/12/24
 * @Description: com.huliyong.controller
 */
@Controller
public class IndexController {

    @Value("classpath:init.json")
    private Resource  resource;

    @GetMapping("/menu")
    public  void data(HttpServletResponse response) throws IOException {
        File file  =resource.getFile();
        FileReader fileReader = new FileReader(file);
        String str;
        StringBuffer sb = new StringBuffer();
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        while ((str = bufferedReader.readLine()) !=null){
            sb.append(str);
        }
        bufferedReader.close();
        fileReader.close();

        response.setContentType("application/json;charset=utf-8");
        response.getWriter().print(sb.toString());
    }

    @GetMapping("/index")
    public String index() {

        System.out.println("-----");
        return "index";
    }
}
