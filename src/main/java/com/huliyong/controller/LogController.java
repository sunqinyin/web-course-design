package com.huliyong.controller;

import com.github.pagehelper.PageInfo;
import com.huliyong.pojo.Admin;
import com.huliyong.pojo.Logtable;
import com.huliyong.service.LogtableService;
//import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLOutput;
import java.util.HashMap;
import java.util.Map;

/**
 * @Auther: H_L_Y
 * @Date: 2020/12/28
 * @Description: com.huliyong.controller
 */
@Controller
@RequestMapping("/log")
public class LogController {
    @Autowired
    private LogtableService logtableService;


    @GetMapping("/toLog")
    public String tolog(){

        return "admin/log";
    }


    @PostMapping("/query")
    @ResponseBody
    public Map<String,Object> query(Logtable logtable, @RequestParam("page") int page , @RequestParam("limit") int limit){

        System.out.println(logtable);

        System.out.println(page+"  "+limit);
        PageInfo pageInfo = logtableService.queryByPage(logtable, page, limit);
        HashMap<String,Object> map = new HashMap<>();
        map.put("code",0);
        map.put("msg","");
        map.put("count",pageInfo.getTotal());
        map.put("data",pageInfo.getList());
        System.out.println(map);
        return  map;
    }

}
