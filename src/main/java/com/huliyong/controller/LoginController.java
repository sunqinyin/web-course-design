package com.huliyong.controller;

import cn.hutool.crypto.SecureUtil;
import com.google.common.base.Strings;
import com.huliyong.pojo.Admin;
import com.huliyong.service.AdminService;
import com.huliyong.utils.MapControl;
import com.huliyong.utils.SessionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
//import sun.security.provider.MD5;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;


@Controller
public class LoginController {

    @Autowired
    private AdminService adminService;

    @GetMapping("/login")
    public  String toLogin(){
        return "login";
    }

    @PostMapping("/login")
    @ResponseBody
    public Map<String, Object> login(@RequestBody Map<String,Object> map, HttpServletRequest request){
        String account = map.get("account")+"";
        String password = map.get("password")+"";
        System.out.println(account+"----"+password);
        String md5paw = SecureUtil.md5(password);
        if(Strings.isNullOrEmpty(account) || Strings.isNullOrEmpty(password)){
            return MapControl.getInstance().error("用户名或密码不能为空").getMap();
        }
        Admin admin = adminService.login(account, md5paw);
        if(admin!=null){
            SessionUtils.setAdmin(request,admin);
            return MapControl.getInstance().success().getMap();
        }else{
            return MapControl.getInstance().error("用户名或密码错误").getMap();
        }
    }






}
