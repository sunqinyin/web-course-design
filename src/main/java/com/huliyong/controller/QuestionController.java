package com.huliyong.controller;

import com.github.pagehelper.PageInfo;
import com.huliyong.pojo.Admin;
import com.huliyong.pojo.Question;
import com.huliyong.pojo.Survey;
import com.huliyong.service.AdminService;
import com.huliyong.service.QuestionService;
import com.huliyong.service.SurveyService;
import com.huliyong.utils.MapControl;
import com.huliyong.utils.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Auther: H_L_Y
 * @Date: 2020/12/26
 * @Description: com.huliyong.controller
 */
@Controller
@RequestMapping("/question")
public class QuestionController {


    @Autowired
    private QuestionService questionService;


    @PostMapping("/create")
    @ResponseBody
    public Map<String,Object> create(@RequestBody Question question, HttpServletRequest request){
        Admin admin = SessionUtils.getAdmin(request);
        question.setCreator(admin.getId());
        int result = questionService.insert(question);
        if(result<=0){
            //失败的情况下
            return MapControl.getInstance().error().getMap();
        }
        return MapControl.getInstance().success().add("id",result).getMap();
    }

    @PostMapping("/query")
    @ResponseBody
    public Map<String,Object> query(@RequestBody Question question, ModelMap modelMap){
        List<Question> list = questionService.queryByPage(question);
        Integer count = questionService.count(question);
        return MapControl.getInstance().page(list,count).getMap();
    }
    @PostMapping("/delete")
    @ResponseBody
    public Map<String,Object> delete(String ids){
        System.out.println(ids);
        int result = questionService.deleteBatch(ids);
        if(result<=0){
            //失败的情况下
            return MapControl.getInstance().error().getMap();
        }
        return MapControl.getInstance().success().getMap();
    }



}
