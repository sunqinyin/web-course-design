package com.huliyong.dao;

import com.huliyong.pojo.Admin;
import com.huliyong.utils.Page;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface AdminDao {

    int deleteByPrimaryKey(Integer id);

    int insert(Admin record);

    int insertSelective(Admin record);

    Admin selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Admin record);

    int updateByPrimaryKey(Admin record);

    Admin login(String account,String password);


    /**
     * 多条件分页查询
     * @param admin
     * @return
     */
    public List<Admin> queryByPage(Admin admin);

    /**
     * 批量删除
     * @param
     * @return
     */
    public int delete(List<Integer> list);
}