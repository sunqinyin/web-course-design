package com.huliyong.dao;

import com.huliyong.pojo.AnswerOpt;

import java.util.List;
import java.util.Map;

public interface AnswerOptDao {
    int deleteByPrimaryKey(Integer id);

    int insert(AnswerOpt record);

    int insertSelective(AnswerOpt record);

    AnswerOpt selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(AnswerOpt record);

    int updateByPrimaryKey(AnswerOpt record);

    List<AnswerOpt> query(Map<String, Object> map);
}