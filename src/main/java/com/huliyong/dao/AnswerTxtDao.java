package com.huliyong.dao;

import com.huliyong.pojo.AnswerTxt;

public interface AnswerTxtDao {
    int deleteByPrimaryKey(Integer id);

    int insert(AnswerTxt record);

    int insertSelective(AnswerTxt record);

    AnswerTxt selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(AnswerTxt record);

    int updateByPrimaryKey(AnswerTxt record);
}