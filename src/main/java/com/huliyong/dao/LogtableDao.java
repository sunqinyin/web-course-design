package com.huliyong.dao;

import com.huliyong.pojo.Admin;
import com.huliyong.pojo.Logtable;

import java.util.List;

public interface LogtableDao {
    int deleteByPrimaryKey(Integer id);

    int insert(Logtable record);

    int insertSelective(Logtable record);

    Logtable selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Logtable record);

    int updateByPrimaryKey(Logtable record);

    List<Logtable> queryByPage(Logtable logtable);

}