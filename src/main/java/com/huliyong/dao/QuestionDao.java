package com.huliyong.dao;

import com.huliyong.pojo.Question;
import com.huliyong.pojo.Survey;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface QuestionDao {
    int deleteByPrimaryKey(Integer id);

    int insert(Question record);

    int insertSelective(Question record);

    Question selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Question record);

    int updateByPrimaryKey(Question record);


    Integer count(Map<String, Object> map);

    List<Question> query(Map<String, Object> map);

    int delete(List<Integer> list);
}