package com.huliyong.dao;

import com.huliyong.pojo.QuestionOpt;
import com.huliyong.pojo.Survey;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface QuestionOptDao {
    int deleteByPrimaryKey(Integer id);

    int insert(QuestionOpt record);

    int insertSelective(QuestionOpt record);

    QuestionOpt selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(QuestionOpt record);

    int updateByPrimaryKey(QuestionOpt record);


    /**
     * 批量删除
     * @param options
     * @return
     */
    int deleteBatch(List<QuestionOpt> options);

    int delete(Map<String, Object> questionId);

    List<QuestionOpt> query(Map<String, Object> surveyId);
}