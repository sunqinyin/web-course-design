package com.huliyong.dao;

import com.huliyong.pojo.Survey;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

public interface SurveyDao {
    int deleteByPrimaryKey(Integer id);

    int insert(Survey record);

    int insertSelective(Survey record);

    Survey selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Survey record);

    int updateByPrimaryKey(Survey record);

    /**
     * 多条件分页查询
     * @param
     * @return
     */
    public List<Survey> queryByPage(Survey survey);

    /**
     * 批量删除
     * @param
     * @return
     */
    public int delete(List<Integer> list);

    public int updateStart(@Param("list") List<Integer> list, @Param("start") String srart);

    List<Integer> queryByState(String start);

    List<Integer> queryByStateForExec(String state);

    List<Survey> query(Map<String, Object> map);
}