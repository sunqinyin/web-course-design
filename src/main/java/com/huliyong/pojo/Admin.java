package com.huliyong.pojo;

import lombok.Data;

import java.io.Serializable;


@Data
public class Admin implements Serializable {
    private Integer id;

    private String account;

    private String password;

    private String name;

    private String phone;

    private String remark;

    private static final long serialVersionUID = 1L;
}