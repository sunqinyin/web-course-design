package com.huliyong.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * tb_answer_txt
 * @author 
 */
@Data
public class AnswerTxt implements Serializable {
    private Integer id;

    private Integer surveyId;

    private Integer questionId;

    private String result;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone= "GMT+8")
    private Date createTime;

    private String voter;

    private static final long serialVersionUID = 1L;
}