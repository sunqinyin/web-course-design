package com.huliyong.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * logtable

 */
@Data
public class Logtable implements Serializable {
    private Integer id;

    private String operateor;

    private String operatetype;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone= "GMT+8")
    private Date operatedate;

    private String operateresult;

    private String remark;

    private static final long serialVersionUID = 1L;
}