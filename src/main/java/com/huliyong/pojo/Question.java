package com.huliyong.pojo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * tb_question
 * @author 
 */
@Data
public class Question implements Serializable {
    private Integer id;

    private String title;

    private String remark;

    /**
     * 1radio|2checkbox|3text|4textarea
     */
    private Integer type;

    /**
     * 0非必填1必填
     */
    private Integer required;

    /**
     * text;number;date
     */
    private String checkStyle;

    /**
     * 0顺序1随机
     */
    private Integer orderStyle;

    /**
     * 1;2;3;4
     */
    private Integer showStyle;

    /**
     * 0不测评1测评
     */
    private Integer test;

    private Integer score;

    private Integer orderby;

    private Integer creator;

    private Date createTime;

    private Integer surveyId;

    private List<QuestionOpt> options;
    private static final long serialVersionUID = 1L;
}