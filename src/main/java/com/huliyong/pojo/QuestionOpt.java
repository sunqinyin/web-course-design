package com.huliyong.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * tb_question_opt
 * @author 
 */
@Data
public class QuestionOpt implements Serializable {
    private Integer id;

    private Integer surveyId;

    private Integer questionId;

    /**
     * 1radio|2checkbox
     */
    private String type;

    private String opt;

    private Integer orderby;

    /**
     * 默认为NULL；1答案
     */
    private Integer answer;

    private static final long serialVersionUID = 1L;

    //被投票数量
    private int num;
}