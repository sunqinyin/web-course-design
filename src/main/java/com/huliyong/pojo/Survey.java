package com.huliyong.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * tb_survey
 * @author 
 */
@Data
public class Survey implements Serializable {

    public static final String state_create="创建";
    public static final String state_exec="执行中";
    public static final String state_over="结束";

    private Integer id;

    private String title;

    private String remark;

    /**
     * 0:不限制;1:限制
     */
    private Integer bounds;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone= "GMT+8")
    private Date startTime;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone= "GMT+8")
    private Date endTime;

    /**
     * 0公开;1密码
     */
    private Integer rules;

    private String password;

    private String url;

    /**
     * 创建、执行中、结束
     */
    private String state;

    private String logo;

    private String bgimg;

    /**
     * 0匿名；1不匿名
     */
    private Integer anon;

    private Integer creator;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone= "GMT+8")
    private Date createTime;

    //创建者信息
    private Admin admin;


    private List<Question> questions;

}