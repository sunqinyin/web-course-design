package com.huliyong.service;

import com.github.pagehelper.PageInfo;
import com.huliyong.pojo.Admin;
import com.huliyong.utils.Page;

import java.util.HashMap;
import java.util.List;


public interface AdminService {

    int deleteByPrimaryKey(Integer id);

    int insert(Admin record);

    int insertSelective(Admin record);

    Admin selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Admin record);

    int updateByPrimaryKey(Admin record);

    Admin login(String account ,String password);


    /**
     * 多条件分页查询
     * @param
     * @return
     */
    public PageInfo queryByPage(Admin admin, Integer pageNum, Integer pageSize);

    /**
     * 批量删除
     * @param ids
     * @return
     */
    int deleteBatch(String ids);
}
