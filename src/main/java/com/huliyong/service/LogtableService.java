package com.huliyong.service;

import com.github.pagehelper.PageInfo;
import com.huliyong.pojo.Logtable;

import java.sql.SQLException;

/**
 * 日志Service
 */
public interface LogtableService {

    public boolean addLog(Logtable log) throws SQLException;

    PageInfo queryByPage(Logtable logtable, int page, int limit);
}
