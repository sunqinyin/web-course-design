package com.huliyong.service;

import com.huliyong.pojo.Question;
import com.huliyong.pojo.Question;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Auther: H_L_Y
 * @Date: 2020/12/27
 * @Description: com.huliyong.service
 */
public interface QuestionService {
    int deleteByPrimaryKey(Integer id);

    int insert(Question record);

    int insertSelective(Question record);

    Question selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Question record);

    int updateByPrimaryKey(Question record);

    /**
     * 多条件分页查询
     * @param
     * @return
     */
    public List<Question> queryByPage(Question Question);



    /**
     * 批量删除
     * @param
     * @return
     */
    public int delete(List<Integer> list);

    public int updateStart(@Param("list") List<Integer> list, @Param("start") String srart);

    List<Integer> queryByState(String start);

    List<Integer> queryByStateForExec(String state);

    Integer count(Question question);

    int deleteBatch(String ids);



}
