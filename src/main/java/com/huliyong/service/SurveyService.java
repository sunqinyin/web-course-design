package com.huliyong.service;

import com.github.pagehelper.PageInfo;
import com.huliyong.pojo.AnswerOpt;
import com.huliyong.pojo.AnswerTxt;
import com.huliyong.pojo.Survey;

import java.util.List;

/**
 * @Auther: H_L_Y
 * @Date: 2020/12/26
 * @Description: com.huliyong.service
 */
public interface SurveyService {

    int deleteByPrimaryKey(Integer id);

    int insert(Survey record);

    int insertSelective(Survey record);

    Survey selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Survey record);

    int updateByPrimaryKey(Survey record);




    /**
     * 多条件分页查询
     * @param
     * @return
     */
    public PageInfo queryByPage(Survey Survey, Integer pageNum, Integer pageSize);

    /**
     * 批量删除
     * @param ids
     * @return
     */
    int deleteBatch(String ids);


    public void updateState();

    List<Survey> queryAll(Survey param);

    Integer submit(List<AnswerOpt> optList, List<AnswerTxt> txtList);

    List<AnswerOpt> queryAnswerOpt(AnswerOpt answerOpt);
}
