package com.huliyong.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.base.Splitter;
import com.huliyong.AOPAnnotation.LogAnno;
import com.huliyong.dao.AdminDao;
import com.huliyong.pojo.Admin;
import com.huliyong.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class AdminServiceImpl implements AdminService {

    @Autowired
    private AdminDao adminDao;
    @LogAnno(operateType = "删除用户")
    @Override
    public int deleteByPrimaryKey(Integer id) {
        return adminDao.deleteByPrimaryKey(id);
    }
    @LogAnno(operateType = "添加用户")
    @Override
    public int insert(Admin record) {
        return adminDao.insert(record);
    }
    @LogAnno(operateType = "添加用户")
    @Override
    public int insertSelective(Admin record) {
        return adminDao.insertSelective(record);
    }
/*
    @Override{
        "resource": "/e:/My-CG-Project-master/My-CG-Project-master/.vscode/c_cpp_properties.json",{
                "resource": "/e:/My-CG-Project-master/My-CG-Project-master/.vscode/c_cpp_properties.json",
                "owner": "e:\\My-CG-Project-master\\My-CG-Project-master",
                "severity": 4,
                "message": "无法找到“${vcpkgRoot}\\x64-windows\\include”。",
                "startLineNumber": 7,
                "startColumn": 16,
                "endLineNumber": 7,
                "endColumn": 50
}
                "owner": "e:\\My-CG-Project-master\\My-CG-Project-master",
                "severity": 4,
                "message": "无法找到“${vcpkgRoot}\\x64-windows\\include”。",
                "startLineNumber": 7,
                "startColumn": 16,
                "endLineNumber": 7,
                "endColumn": 50
    }*/
    public Admin selectByPrimaryKey(Integer id) {
        return adminDao.selectByPrimaryKey(id);
    }
    @LogAnno(operateType = "用户更新")
    @Override
    public int updateByPrimaryKeySelective(Admin record) {
        return adminDao.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Admin record) {
        return adminDao.updateByPrimaryKey(record);

    }
    @LogAnno(operateType = "用户登录")
    @Override
    public Admin login(String account, String password) {
        return adminDao.login(account,password);
    }

    @Override
    public PageInfo queryByPage(Admin admin, Integer pageNum, Integer pageSize) {
//利用PageHelper分页查询 注意：这个一定要放查询语句的前一行,
// 否则无法进行分页,因为它对紧随其后第一个sql语句有效
        PageHelper.startPage(pageNum, pageSize);
        List<Admin> list = adminDao.queryByPage(admin);
        PageInfo<Admin> pageInfo = new PageInfo<Admin>(list);
        return pageInfo ;
    }
    @LogAnno(operateType = "删除用户")
    @Override
    public int deleteBatch(String ids) {
        int flag = 0;
        List<String> list = Splitter.on(",").splitToList(ids);
        List<Integer> collect = list.stream().map(Integer::parseInt).collect(Collectors.toList());

        list.forEach(System.out::println);
        return    adminDao.delete(collect);

    }


}
