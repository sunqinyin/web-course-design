package com.huliyong.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.huliyong.dao.LogtableDao;
import com.huliyong.pojo.Admin;
import com.huliyong.pojo.Logtable;
import com.huliyong.service.LogtableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

@Service
public class LogtableServiceImpl implements LogtableService {

    @Autowired
    private LogtableDao logtableDao;

    @Override
    public boolean addLog(Logtable log) throws SQLException {
        return logtableDao.insert(log) > 0?true:false;
    }

    @Override
    public PageInfo queryByPage(Logtable logtable, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Logtable> list = logtableDao.queryByPage(logtable);
        PageInfo<Logtable> pageInfo = new PageInfo<Logtable>(list);
        return pageInfo ;
    }
}
