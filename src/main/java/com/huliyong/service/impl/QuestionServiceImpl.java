package com.huliyong.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.base.Splitter;
import com.huliyong.dao.QuestionDao;
import com.huliyong.dao.QuestionOptDao;
import com.huliyong.pojo.Question;

import com.huliyong.pojo.QuestionOpt;
import com.huliyong.service.QuestionService;
import com.huliyong.utils.BeanMapUtils;
import com.huliyong.utils.MapParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;



@Service
public class QuestionServiceImpl  implements QuestionService {

    @Autowired
    private QuestionDao questionDao;
    @Autowired
    private QuestionOptDao questionOptDao;
    @Override
    public int deleteByPrimaryKey(Integer id) {
        return questionDao.deleteByPrimaryKey(id);
    }

    @Override
    public int  insert(Question record) {


        int flag = 0;
        if(record.getId()!= null){
            flag = questionDao.updateByPrimaryKeySelective(record);
            List<QuestionOpt> options = record.getOptions();

            options.forEach(System.out::println);
            questionOptDao.delete(MapParameter.getInstance().add("questionId",record.getId()).getMap());
            /*questionOptDao.deleteBatch(options);*/
        }else{
            flag = questionDao.insert(record);
        }
        if(flag>0){
            List<QuestionOpt> options = record.getOptions();
            int i = 0;
            for (QuestionOpt option : options) {
                option.setSurveyId(record.getSurveyId());
                option.setQuestionId(record.getId());
                option.setOrderby(++i);
                questionOptDao.insert(option);
            }
        }
        return record.getId();
    }

    @Override
    public int insertSelective(Question record) {
        return questionDao.insertSelective(record);
    }

    @Override
    public Question selectByPrimaryKey(Integer id) {
        return questionDao.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(Question record) {
        return questionDao.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Question record) {
        return questionDao.updateByPrimaryKey(record);
    }



    @Override
    public List<Question> queryByPage(Question question) {
        //仅仅查询的问题
        Map<String, Object> map = MapParameter.getInstance().put(BeanMapUtils.beanToMap(question)).getMap();
        List<Question> questionList = questionDao.query(map);
        List<QuestionOpt> optList = questionOptDao.query(MapParameter.getInstance().add("surveyId", question.getSurveyId()).getMap());
        for (Question question1 : questionList) {
            List<QuestionOpt> options = new ArrayList<>();
            for (QuestionOpt questionOpt : optList) {
                System.out.println(question1.getId()+"--------------"+questionOpt.getQuestionId());
                if(question1.getId()==questionOpt.getQuestionId()){
                    options.add(questionOpt);
                    System.out.println(question+"===========================================");
                }
            }
            question1.setOptions(options);

        }
        return questionList;
    }

    @Override
    public int delete(List<Integer> list) {
        return 0;
    }

    @Override
    public int updateStart(List<Integer> list, String srart) {
        return 0;
    }

    @Override
    public List<Integer> queryByState(String start) {
        return null;
    }

    @Override
    public List<Integer> queryByStateForExec(String state) {
        return null;
    }

    @Override
    public Integer count(Question question) {

        Map<String, Object> map = MapParameter.getInstance().put(BeanMapUtils.beanToMap(question)).getMap();
        return questionDao.count(map);

    }

    @Override
    public int deleteBatch(String ids) {
        int flag = 0;
        List<String> list = Splitter.on(",").splitToList(ids);
        List<Integer> collect = list.stream().map(Integer::parseInt).collect(Collectors.toList());

        list.forEach(System.out::println);
        return    questionDao.delete(collect);
    }


}
