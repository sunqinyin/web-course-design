package com.huliyong.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.base.Splitter;
import com.huliyong.dao.AnswerOptDao;
import com.huliyong.dao.AnswerTxtDao;
import com.huliyong.dao.SurveyDao;
import com.huliyong.pojo.AnswerOpt;
import com.huliyong.pojo.AnswerTxt;
import com.huliyong.pojo.Survey;
import com.huliyong.service.SurveyService;
import com.huliyong.utils.BeanMapUtils;
import com.huliyong.utils.MapParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Auther: H_L_Y
 * @Date: 2020/12/26
 * @Description: com.huliyong.service.impl
 */
@Service
public class SurveyServiceImpl  implements SurveyService {

    @Autowired
    private SurveyDao surveyDao;
    @Autowired
    private AnswerOptDao answerOptDao;

    @Autowired
    private AnswerTxtDao answerTxtDao;
    @Override
    public int deleteByPrimaryKey(Integer id) {

        return surveyDao.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Survey record) {
        return surveyDao.insert(record);
    }

    @Override
    public int insertSelective(Survey record) {
        return surveyDao.insertSelective(record);
    }

    @Override
    public Survey selectByPrimaryKey(Integer id) {
        return surveyDao.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(Survey record) {
        return surveyDao.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Survey record) {
        return surveyDao.updateByPrimaryKey(record);
    }



    @Override
    public PageInfo queryByPage(Survey survey, Integer pageNum, Integer pageSize) {
//利用PageHelper分页查询 注意：这个一定要放查询语句的前一行,
// 否则无法进行分页,因为它对紧随其后第一个sql语句有效
        PageHelper.startPage(pageNum, pageSize);
        List<Survey> list = surveyDao.queryByPage(survey);
        list.forEach(System.out::println);
        PageInfo<Survey> pageInfo = new PageInfo<Survey>(list);
        return pageInfo ;
    }

    @Override
    public int deleteBatch(String ids) {
        int flag = 0;
        List<String> list = Splitter.on(",").splitToList(ids);
        List<Integer> collect = list.stream().map(Integer::parseInt).collect(Collectors.toList());

        list.forEach(System.out::println);
        return    surveyDao.delete(collect);

    }

    @Override
    public void updateState() {
        List<Integer> list = surveyDao.queryByState(Survey.state_create);
        Survey survey =null;
        if (list.size()>0){
            System.out.println(list);
            surveyDao.updateStart(list,Survey.state_exec);
        }


        List<Integer> list2 = surveyDao.queryByStateForExec(Survey.state_exec);
        if (list2.size()>0){
            System.out.println(list2);
            surveyDao.updateStart(list2 ,Survey.state_over);
        }

    }

    @Override
    public List<Survey> queryAll(Survey survey){
        Map<String, Object> map = MapParameter.getInstance().put(BeanMapUtils.beanToMap(survey)).getMap();
        return surveyDao.query(map);
    }

    @Override
    public Integer submit(List<AnswerOpt> optList, List<AnswerTxt> txtList) {
        int flag = 0;
        for (AnswerOpt opt : optList) {
            flag = answerOptDao.insert(opt);
        }
        for (AnswerTxt txt : txtList) {
            flag = answerTxtDao.insert(txt);
        }
        return flag;
    }

    @Override
    public List<AnswerOpt> queryAnswerOpt(AnswerOpt answerOpt) {
        Map<String, Object> map = MapParameter.getInstance().put(BeanMapUtils.beanToMap(answerOpt)).getMap();
        return answerOptDao.query(map);
    }

}
