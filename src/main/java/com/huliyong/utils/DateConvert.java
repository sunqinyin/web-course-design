package com.huliyong.utils;


import org.springframework.core.convert.converter.Converter;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateConvert implements Converter<String,Date> {

    @Override
    public Date convert(String arg0) {
        Date date=null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");//设置时间格式
            return date = sdf.parse(arg0);
        } catch (Exception e) {
        }
        return date;
    }
}
