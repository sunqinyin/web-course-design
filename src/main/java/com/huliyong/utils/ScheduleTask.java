package com.huliyong.utils;

import com.huliyong.service.SurveyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @Auther: H_L_Y
 * @Date: 2020/12/27
 * @Description: com.huliyong.utils
 * 定时任务
 */
@EnableScheduling
public class ScheduleTask {


    @Autowired
    private SurveyService surveyService;
    /**
     * 调查问卷状态的任务
     */
    @Scheduled(fixedRate=10000)
//    @Scheduled(cron = "* * */1 * * ?")
    public void state(){
        System.out.println("执行任务....");
        surveyService.updateState();
    }
}
